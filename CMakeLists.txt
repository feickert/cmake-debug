cmake_minimum_required (VERSION 3.6 FATAL_ERROR)
set(CMAKE_CXX_STANDARD 14)

# Try to figure out what project is our parent. Just using a hard-coded list
# of possible project names. Basically the names of all the other
# sub-directories inside the Projects/ directory in the repository.
set( _parentProjectNames Athena AthenaP1 AnalysisBase AthAnalysis
   AthSimulation AthDerivation AnalysisTop )
set( _defaultParentProject AnalysisBase )
foreach( _pp ${_parentProjectNames} )
   if( NOT "$ENV{${_pp}_DIR}" STREQUAL "" )
      set( _defaultParentProject ${_pp} )
      break()
   endif()
endforeach()

# Set the parent project name based on the previous findings:
set( ATLAS_PROJECT ${_defaultParentProject}
   CACHE STRING "The name of the parent project to build against" )
# Clean up:
unset( _parentProjectNames )
unset( _defaultParentProject )

find_package( ${ATLAS_PROJECT} REQUIRED )

set(ATLAS_PROJECT_VERSION "${${ATLAS_PROJECT}_VERSION}")
message("")
message("-- ATLAS_PROJECT: ${ATLAS_PROJECT} ${ATLAS_PROJECT_VERSION}")
message("")

# Set up CTest. This makes sure that per-package build log files can be
# created if the user so chooses.
atlas_ctest_setup()


atlas_project( TestLibrary 1.0.0
   USE ${ATLAS_PROJECT} ${ATLAS_PROJECT_VERSION} )
message("")

# Set up the runtime environment setup script. This makes sure that the
# project's "setup.sh" script can set up a fully functional runtime environment,
# including all the externals that the project uses.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
  DESTINATION . )

# Set up CPack. This call makes sure that an RPM or TGZ file can be created
# from the built project. Used by Panda to send the project to the grid worker
# nodes.
atlas_cpack_setup()

option(USE_EXTERNAL_JSON "" OFF)
