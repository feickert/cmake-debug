#!/usr/bin/env bash

function print_and_run () {
    printf "\n# %s\n" "${1}"
    eval $(echo "$1")
}

function setNumProcessors () {
    # Set the number of processors used for build
    # to be 1 less than are available
    if [[ -f "$(which nproc)" ]]; then
        NPROC="$(nproc)"
    else
        NPROC="$(grep -c '^processor' /proc/cpuinfo)"
    fi
    echo `expr "${NPROC}" - 1`
}

if [ "$(basename -- $0)" = "$(basename ${BASH_SOURCE})" ]; then
    # running in subshell
    # Define setupATLAS and asetup
    ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
    function setupATLAS {
        source "${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh"
    }
fi

function envSetup {
    baseDir=${PWD}
    print_and_run "setupATLAS"
    print_and_run "lsetup python git asetup"
}

function build {
    # AtlasRelease="latest"
    # https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/bbvv/CxAODFramework_HHbbVV/CxAODBootstrap_HHbbVV/-/blob/master/bootstrap/release.txt
    AtlasRelease="21.2.114"
    echo "Target ATLAS Release will be ${AtlasRelease}."

    if [[ -f "${baseDir}/src/.asetup.save" ]]; then
      rm "${baseDir}/src/.asetup.save"
    fi
    cd "${baseDir}/src"
    print_and_run "asetup ${AtlasRelease},AnalysisBase"
    cd "${baseDir}"

    # Force clean build
    if [[ -d "${baseDir}/build" ]]; then
      rm -rf "${baseDir}/build"
    fi
    print_and_run "mkdir -p build"

    cd "${baseDir}"
    print_and_run "cd build"
    print_and_run "cmake .."

    NPROC="$(setNumProcessors)"
    printf "\n# %s\n" "cmake --build . -- -j${NPROC}"
    cmake --build . -- -j${NPROC}

    # # echo "Build has ended at "$(tail -n 1 ${baseDir}/build/CMake_out.txt | sed 's/].*$/]/')" complete"
    # cd ..
    # printf "\n### Run: source "$(find ./build -iname "setup.sh" | sed 's/.\///')"\n"
    # source build/*/setup.sh
    # echo ""
}

# Run program
envSetup
build
